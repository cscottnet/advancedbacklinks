<?php

class ApiQueryAbLinkStatsProp extends ApiQueryBase {

	/**
	 * ApiQueryAbLinkStatsProp constructor.
	 * @param ApiQuery $query
	 * @param $moduleName
	 */
	public function __construct( ApiQuery $query, $moduleName ) {
		parent::__construct( $query, $moduleName, 'abls' );
	}

	/**
	 * @inheritDoc
	 * @throws ApiUsageException
	 */
	public function execute() {
		$titles = $this->getPageSet()->getGoodTitles();
		$result = $this->getResult();
		$params = $this->extractRequestParams();

		foreach ( $titles as $title ) {
			$id = $title->getArticleID();
			if ( $id == 0 ) continue;

			$result->addValue(
				[ 'query', 'pages', $id ],
				$this->getModuleName(),
				AdvancedBacklinksUtils::GetLinkStats( $title, $params['directonly'], $params['contentonly'],
					$params['redirects'], $params['throughredirects'] )
			);
		}
	}

	public function getAllowedParams() {
		return [
			'directonly' => [
				ApiBase::PARAM_TYPE => 'boolean',
				ApiBase::PARAM_DFLT => false
			],
			'contentonly' => [
				ApiBase::PARAM_TYPE => 'boolean',
				ApiBase::PARAM_DFLT => false
			],
			'redirects' => [
				ApiBase::PARAM_TYPE => 'boolean',
				ApiBase::PARAM_DFLT => false
			],
			'throughredirects' => [
				ApiBase::PARAM_TYPE => 'boolean',
				ApiBase::PARAM_DFLT => false
			]
		];
	}

	/**
	 * @param array $params
	 * @return string
	 */
	public function getCacheMode( $params ) {
		return 'public';
	}
}

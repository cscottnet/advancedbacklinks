# AdvancedBacklinks

A MediaWiki extension for advanced backlink tracking, including tracking backlinks from wikitext only.

Current master branch requires this not-yet-merged patch in MediaWiki to function: https://gerrit.wikimedia.org/r/#/c/mediawiki/core/+/535123/
If you are looking for the version of this extension using Parsoid, see the REL1_33 branch.
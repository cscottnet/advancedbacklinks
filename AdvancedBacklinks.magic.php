<?php
/**
 * AdvancedBacklinks
 * Copyright (C) 2019  Ostrzyciel
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

$magicWords = [];

/** English (English) */
$magicWords['en'] = [
	'directlink' => [ '1', '__DIRECTLINK__' ],
	'redlinkallergic' => [ '1', '__REDLINKALLERGIC__' ],
	'ignoreorphaned' => [ '1', '__IGNOREORPHANED__' ],
	'redlinkallergicthrough' => [ '1', '__REDLINKALLERGICTHROUGH__' ],
	'transclusionisnotadoption' => [ '1', '__TRANSCLUSIONISNOTADOPTION__' ],
];

/** Polski (Polish) */
$magicWords['pl'] = [
	'redlinkallergic' => [ '1', '__ALERGIANAREDLINKI__' ],
	'ignoreorphaned' => [ '1', '__IGNORUJOSIEROCONE__', '__NIEBĄDŹSIEROTĄ__' ],
	'redlinkallergicthrough' => [ '1', '__ALERGIANAREDLINKIPRZEZ__' ],
	'transclusionisnotadoption' => [ '1', '__TRANSKLUZJATONIEADOPCJA__' ],
];